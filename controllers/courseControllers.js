const Course = require("../models/Course");

//Mini Activity
//Create a new course
/*
    Steps:
    1.Create a new Course object using the mongoose model and the information from the reqBody
        name
        description
        price
    2.Save the new User to the database
    3. Send a screenshot of 3 courses from your database

*/
// module.exports.addCourse = (reqBody) => {

//  // Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
//  // Uses the information from the request body to provide all the necessary information
//  let newCourse = new Course({
//      name : reqBody.name,
//      description : reqBody.description,
//      price : reqBody.price
//  });

//  // Saves the created object to our database
//  return newCourse.save().then((course, error) => {

//      // Course creation successful
//      if (error) {

//          return false;

//      // Course creation failed
//      } else {

//          return true;

//      };

//  });

// };

      // module.exports.addCourse = (reqBody)=>{

      // let newCourse = new Course({
      // name : reqBody.course.name,
      // description : reqBody.course.description,
      // price : reqBody.course.price
      // });     
      // return new Promise(resolve =>{
      // if (reqBody.isAdmin === false){
      // resolve('not an admin!');
      // }else {
      // newCourse.save().then((savedata, reject)=>{
      // resolve("Success!")
      // })
      // }
      // })

      // }
//solution 1
// module.exports.addCourse = (reqBody) => {
//     let newCourse = new Course({
//         name : reqBody.name,
//         description : reqBody.description,
//         price : reqBody.price
//     });
//     if(reqBody.isAdmin){

//         return newCourse.save().then((course,error) =>{
//             if (error){
//                 return false;
//             }else {
//                 return true;
//             };
//         });
//     }else{
//         return false;
//     }
// };

//solution 2
      module.exports.addCourse = (data) => {
        if(data.isAdmin){
            let newCourse = new Course({
                name : data.course.name,
                description : data.course.description,
                price : data.course.price
            });
            return newCourse.save().then((course,error) =>{
                if (error) {
                    return false;
                }else {
                    return true;
                }
            })
        }else {
            return true
        };
      };

// Retrieve all courses
/*
    1.Retrieve all the courses from the database
        MOdel.find({})
*/

module.exports.getAllCourses = ()=>{
    return Course.find({}).then(result=>{ // if you put two curly braces it means all documents
        return result;
    })
}

// Retrieve all Active Courses
/*
    1. Retrieve all the courses from the database with the property of "isActive" to true

*/

module.exports.getAllActive = ()=>{
    return Course.find({isActive:true}).then(result=>{ // all active should be true yung :
        return result;
    })
}


//Retrieving a specific course

/*
    1. Retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (reqParams)=>{
    return Course.findById(reqParams.courseId).then(result=>{
        return result;
    });
};

// Update a course

/*
    1. Create a variable "updatedCOurse" which will contain the information retrieved from the request body
    2. Find and update the course using the course ID retrieve from the request params property and the variable "updatedCourse" containing the information from the request body

*/

module.exports.updateCourse = (reqParams,reqBody)=>{ // you can put anyname of params basta magkasunod
        let updatedCourse = {
            name : reqBody.name,
            description: reqBody.description,
            price : reqBody.price
        };
        return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error)=>{
            if(error){
                return false;
            }else{
                return true;
            }
        })
}

// Archive a course
/*
    1. Make a course active or inactive
*/

// module.exports.archiveCourse = (reqParams,reqBody)=>{
//         let archivedCourse = {isActive : reqBody.isActive};
//         return Course.findByIdAndUpdate(reqParams.courseId,archivedCourse).then((course,error)=>{
//             if(error){
//                 return false;
//             }else{
//                 return true;
//             };
//         });
// }

//sol 1

// module.exports.archiveCourse = (reqParams) =>{
//     console.log(reqParams);
//     let updateActivefield
// }

// sol2
module.exports.archiveCourse = (reqParams)=>{
    return Course.findByIdAndUpdate(reqParams,{isActive:false}).then((course,error)=>{
        if(error){
            return false;
        }else {
            return true;
        }
    })
}

// stretch goal
//to reactivate
module.exports.activateCourse = (reqParams)=>{
    return Course.findByIdAndUpdate(reqParams,{isActive:true}).then((course,error)=>{
        if(error){
            return false;
        }else {
            return true;
        }
    })
}
